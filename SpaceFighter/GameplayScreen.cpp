
#include "GameplayScreen.h"
#include "Level.h"
#include "Level01.h"

GameplayScreen::GameplayScreen(const int levelIndex)
{
	//At start, level identified is null.
	m_pLevel = nullptr;
	//This switch will set the level based on current levelindex set in the parameter.
	switch (levelIndex)
	{
	case 0: m_pLevel = new Level01(); break;
	}

	//Transition times between fading into gameplay and out of gameplay.
	SetTransitionInTime(1.0f);
	SetTransitionOutTime(0.5f);

	//Katana Engine Method: Based on settings within GameplayScreen method, will make the screen transition in.
	Show();
}

void GameplayScreen::LoadContent(ResourceManager *pResourceManager)
{
	m_pLevel->LoadContent(pResourceManager);
}

void GameplayScreen::HandleInput(const InputState *pInput)
{
	m_pLevel->HandleInput(pInput);
}

void GameplayScreen::Update(const GameTime *pGameTime)
{
	m_pLevel->Update(pGameTime);
}

void GameplayScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();

	m_pLevel->Draw(pSpriteBatch);

	pSpriteBatch->End();
}
