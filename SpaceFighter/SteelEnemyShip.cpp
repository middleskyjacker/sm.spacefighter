#include "SteelEnemyShip.h"
#include "Blaster.h"

SteelEnemyShip::SteelEnemyShip() {
	SetSpeed(50);
	SetMaxHitPoints(3);
	SetCollisionRadius(20);
}

void SteelEnemyShip::Update(const GameTime* pGameTime) {

	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());
		EnemyFireWeapons(TriggerType::PRIMARY);

		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}

void SteelEnemyShip::Draw(SpriteBatch* pSpriteBatch) {
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}