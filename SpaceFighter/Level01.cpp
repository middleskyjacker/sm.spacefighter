

#include "Level01.h"
#include "BioEnemyShip.h"
#include "SteelEnemyShip.h"
#include "Blaster.h"

void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");
	Texture *sTexture = pResourceManager->Load<Texture>("Textures\\BlueSteelEnemyShip.png");

	const int COUNT = 21;
	const int SCOUNT = 1;

	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};
	
	double xSPos[COUNT] = {
		0.5
	};

	/*double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3
	};*/ //Defaults

	double delays[COUNT] =
	{
		0.0, 0.20, 0.20,
		2.0, 0.25, 0.25,
		2.5, 0.15, 0.15, 0.15, 0.20,
		3.0, 0.25, 0.25, 0.25, 0.25,
		0.2, 0.2, 0.2, 0.2, 0.2
	};

	double sDelays[COUNT] = {
		2
	};

	float delay = 3.0; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BioEnemyShip *pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}
	SteelEnemyShip* pEnemy;
	for (int i = 0; i < SCOUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

        pEnemy = new SteelEnemyShip();
		pEnemy->SetTexture(sTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		Blaster* pEnemyBlaster = new Blaster(true);
		pEnemyBlaster->SetIsEnemy();
		pEnemyBlaster->SetCooldownSeconds(1);
		pEnemyBlaster->SetProjectilePool(&m_eprojectiles);
		pEnemy->AttachWeapon(pEnemyBlaster, Vector2::UNIT_Y * -20);
		AddGameObject(pEnemy);
	}
	//Create enemy projectiles (Level.cpp for reference)
	for (int i = 0; i < 100; i++)
	{
		EnemyProjectile* eProjectile = new EnemyProjectile();
		m_eprojectiles.push_back(eProjectile);
		AddGameObject(eProjectile);
	}

	Level::LoadContent(pResourceManager);
}

