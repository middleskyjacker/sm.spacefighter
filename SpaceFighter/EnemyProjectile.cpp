#include "EnemyProjectile.h"

Texture* EnemyProjectile::s_pTexture = nullptr;

EnemyProjectile::EnemyProjectile()
{
	SetSpeed(500);
	SetDamage(1);
	SetDirection(Vector2::UNIT_Y);
	SetCollisionRadius(9);

	m_drawnByLevel = true;
}

void EnemyProjectile::Update(const GameTime* pGameTime)
{
	if (IsActive())
	{
		Vector2 translation = m_direction * m_speed * pGameTime->GetTimeElapsed();
		TranslatePosition(translation);

		Vector2 position = GetPosition();
		Vector2 size = s_pTexture->GetSize();

		// Is the projectile off the screen?
		if (position.Y < -size.Y) Deactivate();
		else if (position.X < -size.X) Deactivate();
		else if (position.Y > Game::GetScreenHeight() + size.Y) Deactivate();
		else if (position.X > Game::GetScreenWidth() + size.X) Deactivate();
	}

	GameObject::Update(pGameTime);
}

void EnemyProjectile::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(s_pTexture, GetPosition(), Color::White, s_pTexture->GetCenter());
	}
}

void EnemyProjectile::Activate(const Vector2& position, bool wasShotByEnemy)
{
	m_wasShotByEnemy = wasShotByEnemy;
	SetPosition(position);

	GameObject::Activate();
}

std::string EnemyProjectile::ToString() const
{
	return ((WasShotByEnemy()) ? "Player " : "Enemy ") + GetProjectileTypeString();
}

CollisionType EnemyProjectile::GetCollisionType() const
{
	CollisionType shipType = WasShotByEnemy() ? CollisionType::ENEMY : CollisionType::PLAYER;
	return (shipType | GetProjectileType());
}

