#pragma once

#include "EnemyShip.h"

class SteelEnemyShip : public EnemyShip
{

public:

	SteelEnemyShip();
	virtual ~SteelEnemyShip() { }

	virtual void SetTexture(Texture* pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);

private:

	Texture* m_pTexture = nullptr;

	std::vector<Weapon*> m_weapons;
	std::vector<Weapon*>::iterator m_weaponIt;

};

