
#pragma once

#include "Weapon.h"

class Blaster : public Weapon
{

public:

	Blaster(const bool isActive) : Weapon(isActive)
	{
		m_cooldown = 0;
		//m_cooldownSeconds = 0.35; //Default
		m_cooldownSeconds = 0.1;
	}

	virtual ~Blaster() { }

	virtual void Update(const GameTime *pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}

	virtual bool CanFire() const { return m_cooldown <= 0; }

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	virtual void Fire(TriggerType triggerType)
	{
		if (IsActive() && CanFire())
		{
			if (triggerType.Contains(GetTriggerType()))
			{
				Projectile *pProjectile = GetProjectile();
				if (pProjectile)
				{
					pProjectile->Activate(GetPosition(), m_isPlayer);
					m_cooldown = m_cooldownSeconds;
				}
			}
		}
	}
	virtual void EnemyFire(TriggerType triggerType)
	{
		if (IsActive() && CanFire())
		{
			if (triggerType.Contains(GetTriggerType()))
			{
				EnemyProjectile* pEProjectile = GetEProjectile();
				if (pEProjectile)
				{
					pEProjectile->Activate(GetPosition(), m_isEnemy);
					m_cooldown = m_cooldownSeconds;
				}
			}
		}
	}
	virtual void SetIsEnemy() {
		m_isEnemy = true;
		m_isPlayer = false;
	}

private:

	float m_cooldown;
	float m_cooldownSeconds;
	bool m_isEnemy = false;
	bool m_isPlayer = true;

};